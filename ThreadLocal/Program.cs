﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ThreadLocal
{
	class Program
	{
		static ThreadLocal<Tuple<DateTime,string>> local;
		//static LocalDataStoreSlot slot = Thread.AllocateDataSlot();
		static void Main(string[] args)
		{
			local = new ThreadLocal<Tuple<DateTime, string>>();

			//修改TLS的线程
			for (int i = 0; i < 5; i++)
			{
				Thread th = new Thread(() =>
				{

					local.Value = Tuple.Create<DateTime, string>(DateTime.Now, i.ToString());

					Display();

				});



				th.Start();

				th.Join();
			}

			Console.ReadLine();
			//Display();
		
		}
		static void Display()
		{
			Thread.Sleep(new Random(2000).Next(5000));
			Console.WriteLine("{0} {1} {2}", Thread.CurrentThread.ManagedThreadId, local.Value.Item1,local.Value.Item2);

		}
	}
}
