﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace WindowsFormsApplication1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			
		}

		private void button1_Click(object sender, EventArgs e)
		{for (int i = 0; i < 10; i++)
			{
				new Thread(() => this.Invoke((MethodInvoker)delegate  //Invoke:在拥有此控件的基础窗口句柄的线程上执行委托
            {
                this.label1.Text =string.Format( "Running in a thread, id: {0} {1}", Thread.CurrentThread.ManagedThreadId,i);
				
            })).Start();
						//Console.WriteLine("Running in a thread, id: {0}", Thread.CurrentThread.ManagedThreadId)
					//).Start();
			Thread.Sleep(1000);
			}

		}
	}
}
