﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PhEating
{
    /// <summary>
    /// Copyright (C) 2008-2012 jiayp
    /// 公司网站:
    /// CLR版本:2.0.50727.1433
    /// 机器名称:JIAYP
    /// 开发团队:
    /// 创建时间：10/20/2011 15:45:37
    /// 创建年份: 2011   
    /// 作者：jiayp,qq:104652091
    /// 功能介绍:客户端设备树，用于展开所有节点

    /// 检查时间:
    /// 检查人:
    /// 检查结果:
    /// </summary>
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}