using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
namespace PhEating
{

    /// <summary>
    /// Copyright (C) 2008-2012 jiayp
    /// 
    /// CLR版本:2.0.50727.1433
    /// 机器名称:JIAYP
    /// 开发团队
    /// 创建时间：10/20/2011 15:45:37
    /// 创建年份: 2011   
    /// 作者：jiayp,qq:104652091
    /// 功能介绍:客户端设备树，用于展开所有节点

    /// 检查时间:
    /// 检查人:
    /// 检查结果:
    /// </summary>
    public partial class MainForm : Form
    {
        private Mutex[] m_chopsticksMux;
        private Thread[] m_thread;
        private Label[] m_phInfo;
        private CheckBox[] m_chopInfo;
        private SynchronizationContext m_context;
        private bool m_isRunning = true;
        public MainForm()
        {
            InitializeComponent();
            m_context = SynchronizationContext.Current;
            m_phInfo = new Label[6];
            m_phInfo[0] = label1;
            m_phInfo[1] = label2;
            m_phInfo[2] = label3;
            m_phInfo[3] = label4;
            m_phInfo[4] = label5;
            m_phInfo[5] = label6;
            m_chopInfo = new CheckBox[6];
            m_chopInfo[0] = checkBox1;
            m_chopInfo[1] = checkBox2;
            m_chopInfo[2] = checkBox3;
            m_chopInfo[3] = checkBox4;
            m_chopInfo[4] = checkBox5;
            m_chopInfo[5] = checkBox6;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_chopsticksMux = new Mutex[6];
            m_thread = new Thread[6];
           for(int i=0;i<6;i++)
           {
               m_chopsticksMux[i]=new Mutex(true,"chop sticks"+i.ToString());     
           }         

           ParameterizedThreadStart pts = new ParameterizedThreadStart(Eating);
           m_thread[0] = new Thread(pts);

           for (int i = 1; i < 6; i++)
           {
               pts = new ParameterizedThreadStart(Eating);
               m_thread[i] = new Thread(pts);
           }


           Philospher ph = new Philospher();
           ph.m_name = "Philospher0";
           ph.m_index = 0;

           ChopStick csl = new ChopStick();
           csl.m_number = 5;
           csl.m_mutex = m_chopsticksMux[5];               
           ph.m_left = csl;
         
           ChopStick csr = new ChopStick();
           csr.m_number = 0;
           csr.m_mutex = m_chopsticksMux[0];
           ph.m_right = csr;

           m_thread[0].Start(ph);
           for (int i = 1; i < 6; i++)
           {

               ph = new Philospher();
               ph.m_name = "Philospher" + i.ToString();
               ph.m_index = i;


               csl = new ChopStick();
               csl.m_number = i-1;
               csl.m_mutex = m_chopsticksMux[i - 1];             
               ph.m_left = csl;

               csr = new ChopStick();
               csr.m_number = i;
               csr.m_mutex = m_chopsticksMux[i];
               ph.m_right = csr;         

               m_thread[i].Start(ph);
           }

           for (int i = 0; i < 6; i++)
           {
               m_chopsticksMux[i].ReleaseMutex();
           }
        }

        private void Eating(object para)
        {
            Philospher ph = para as Philospher;
            WaitHandle[] hd = new WaitHandle[2];
            hd[0] = ph.m_left.m_mutex;
            hd[1] = ph.m_right.m_mutex;
            while (true && m_isRunning)
            {
                if (Mutex.WaitAll(hd))
                {
                    StartEating(ph);
                    Thread.Sleep(1500);
                    StopEating(ph);
                    ph.Release();
                   
                }
            }
        }
        private void StartEating(Philospher ph)
        {
            m_context.Post(new SendOrPostCallback(delegate
               {

            m_phInfo[ph.m_index].Text = ph.m_name + ": is eating";
            m_phInfo[ph.m_index].BackColor = Color.Red;
            m_chopInfo[ph.m_left.m_number].Text = "using by "+ph.m_name;
            m_chopInfo[ph.m_left.m_number].BackColor = Color.Green;
            m_chopInfo[ph.m_right.m_number].Text = "using by " + ph.m_name;
            m_chopInfo[ph.m_right.m_number].BackColor = Color.Green;
               }),null);
        }
        private void StopEating(Philospher ph)
        {
            m_context.Post(new SendOrPostCallback(delegate
              {

                  m_phInfo[ph.m_index].Text = ph.m_name + ": stop and wait";
                  m_phInfo[ph.m_index].BackColor = Color.Gray;
                  m_chopInfo[ph.m_left.m_number].Text = "";
                  m_chopInfo[ph.m_right.m_number].Text = "";
                  m_chopInfo[ph.m_left.m_number].BackColor = Color.Gray;
                  m_chopInfo[ph.m_right.m_number].BackColor = Color.Gray;
              }),null);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_isRunning = false;
        }
    }
    /// <summary>
    /// 哲学家对象
    /// </summary>
    public class Philospher
    {
        public String m_name;
        //筷子
        public ChopStick m_left;
        public ChopStick m_right;
        //哲学家编号
        public int m_index;
        //释放
        public void Release()
        {
            m_left.Release();
            m_right.Release();
        }
    }

    public class ChopStick
    {
        public Mutex m_mutex;
        public int m_number;
        public void Release()
        {
            m_mutex.ReleaseMutex();
        }
    }
}