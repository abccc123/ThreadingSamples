﻿using System;
using System.Threading;

public class Example
{
	/**** http://msdn.microsoft.com/zh-cn/library/system.threading.manualresetevent.aspx
	//下面的示例说明ManualResetEvent嵌入的方法: 该示例以未终止状态中的 ManualResetEvent 开始（即 false 传递给构造函数）
	 * 。 该示例创建三个线程，每个线程都可以被 ManualResetEvent 阻止（通过调用其 WaitOne 方法）。
	 * 当按“输入”键时，示例将调用 Set 方法，该方法可释放所有三个线程。 对比该其与 AutoResetEvent 的行为，这释放一次一个线程，在每个释放后自动重新设置。
//再次按“输入”键后，我们会发现 ManualResetEvent在调用其 Reset 方法前会一直保持终止状态：该示例另外启动两个线程。 这些线程不会阻止，如果它们调用 WaitOne方法，而是会运行至完成。
//再次按“输入”键会使该示例调用 Reset 方法并另外启动一个线程，该线程会在它调用 WaitOne 的时候被阻塞。 最后一次按“输入”键将调用 Set 来释放最后一个线程，然后程序结束。
	// mre is used to block and release threads manually. It is
	// created in the unsignaled state.***/
	private static ManualResetEvent mre = new ManualResetEvent(false);

	static void Main()
	{
		Console.WriteLine("\nStart 3 named threads that block on a ManualResetEvent:\n");

		for (int i = 0; i <= 2; i++)
		{
			Thread t = new Thread(ThreadProc);
			t.Name = "Thread_" + i;
			t.Start();
		}

		Thread.Sleep(500);
		Console.WriteLine("\nWhen all three threads have started, press Enter to call Set()" +
						  "\nto release all the threads.\n");
		Console.ReadLine();

		mre.Set();

		Thread.Sleep(500);
		Console.WriteLine("\nWhen a ManualResetEvent is signaled, threads that call WaitOne()" +
						  "\ndo not block. Press Enter to show this.\n");
		Console.ReadLine();

		for (int i = 3; i <= 4; i++)
		{
			Thread t = new Thread(ThreadProc);
			t.Name = "Thread_" + i;
			t.Start();
		}

		Thread.Sleep(500);
		Console.WriteLine("\nPress Enter to call Reset(), so that threads once again block" +
						  "\nwhen they call WaitOne().\n");
		Console.ReadLine();

		mre.Reset();

		// Start a thread that waits on the ManualResetEvent.
		Thread t5 = new Thread(ThreadProc);
		t5.Name = "Thread_5";
		t5.Start();

		Thread.Sleep(500);
		Console.WriteLine("\nPress Enter to call Set() and conclude the demo.");
		Console.ReadLine();

		mre.Set();

		// If you run this example in Visual Studio, uncomment the following line:
		//Console.ReadLine();
	}


	private static void ThreadProc()
	{
		string name = Thread.CurrentThread.Name;

		Console.WriteLine(name + " starts and calls mre.WaitOne()");

		mre.WaitOne();

		Console.WriteLine(name + " ends.");
	}
}