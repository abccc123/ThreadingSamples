﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ManualResetEvent2
{

/***   示例场景：张三、李四两个好朋友去餐馆吃饭，两个人点了一份宫爆鸡丁，宫爆鸡丁做好需要一段时间，张三、李四不愿傻等，都专心致志的玩起了手机游戏，心想宫爆鸡丁做好了，服务员肯定会叫我们的。服务员上菜之后，张三李四开始享用美味的饭菜，饭菜吃光了，他们再叫服务员过来买单。我们可以从这个场景中抽象出来三个线程，张三线程、李四线程和服务员线程，他们之间需要同步：服务员上菜—>张三、李四开始享用宫爆鸡丁—>吃好后叫服务员过来买单。这个同步用什么呢？ ManualResetEvent还是AutoResetEvent？通过上面的分析不难看出，我们应该用ManualResetEvent进行同步，下面是程序代码： 

张三李四吃饭的故事 编译后查看运行结果，符合我们的预期，控制台输出为： 
      服务员：厨师在做菜呢，两位稍等... 
      张三：等着上菜无聊先玩会手机游戏 
      李四：等着上菜无聊先玩会手机游戏 
      张三：等着上菜无聊先玩会手机游戏 
      李四：等着上菜无聊先玩会手机游戏 
      服务员：宫爆鸡丁好了 
      张三：开始吃宫爆鸡丁 
      李四：开始吃宫爆鸡丁 
      张三：宫爆鸡丁吃光了 
      李四：宫爆鸡丁吃光了 
      服务员：两位请买单 

      如果改用AutoResetEvent进行同步呢？会出现什么样的结果？恐怕张三和李四就要打起来了，一个享用了美味的宫爆鸡丁，另一个到要付账的时候却还在玩游戏。感兴趣的朋友可以把注释的那行代码注释去掉，并把下面一行代码注释掉，运行程序看会出现怎样的结果。 
 ***/
public class EventWaitTest  
    {  
        private string name; //顾客姓名  
        //private static AutoResetEvent eventWait = new AutoResetEvent(false);  
        private static ManualResetEvent eventWait = new ManualResetEvent(false);  
        private static ManualResetEvent eventOver = new ManualResetEvent(false);  
  
        public EventWaitTest(string name)  
        {  
            this.name = name;  
        }  
  
        public static void Product()  
        {  
            Console.WriteLine("服务员：厨师在做菜呢，两位稍等");  
            Thread.Sleep(2000);  
            Console.WriteLine("服务员：宫爆鸡丁好了");  
            eventWait.Set();  
            while (true)  
            {  
                if (eventOver.WaitOne(1000, false))  
                {  
                    Console.WriteLine("服务员：两位请买单");  
                    eventOver.Reset();  
                }  
            }  
        }  
  
        public void Consume()  
        {  
            while (true)  
            {  
                if (eventWait.WaitOne(1000, false))  
                {  
                    Console.WriteLine(this.name + "：开始吃宫爆鸡丁");  
                    Thread.Sleep(2000);  
                    Console.WriteLine(this.name + "：宫爆鸡丁吃光了");  
                    eventWait.Reset();  
                    eventOver.Set();  
                    break;  
                }  
                else  
                {  
                    Console.WriteLine(this.name + "：等着上菜无聊先玩会手机游戏");  
                }  
            }  
        }  
    }  
  
    public class App  
    {  
        public static void Main22(string[] args)  
        {  
            EventWaitTest zhangsan = new EventWaitTest("张三");  
            EventWaitTest lisi = new EventWaitTest("李四");  
  
            Thread t1 = new Thread(new ThreadStart(zhangsan.Consume));  
            Thread t2 = new Thread(new ThreadStart(lisi.Consume));  
            Thread t3 = new Thread(new ThreadStart(EventWaitTest.Product));  
  
            t1.Start();  
            t2.Start();  
            t3.Start();  
  
            Console.Read();           
        }  
    }  
}
