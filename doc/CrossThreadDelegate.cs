﻿   using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;
namespace WinFormsTerminal.comm
{
    /******this.Invoke((EventHandler)delegate
{
    //TextBox1.Text="";
});
     * 
     *  Invoke(new MethodInvoker(delegate
    {
        this.text = "New Text";
    }));
 ********/

    /// <summary>
    /// 线程中安全访问控件，避免重复的delegate,Invoke
    /// </summary>
    public static class CrossThreadCalls
    {
        public delegate void TaskDelegate();

        private delegate void InvokeMethodDelegate(Control control, TaskDelegate handler);

        /// <summary>
        /// .net2.0中线程安全访问控件扩展方法，可以获取返回值，可能还有其它问题
        /// </summary>
        /// CrossThreadCalls.SafeInvoke(this.statusStrip1, new CrossThreadCalls.TaskDelegate(delegate()
        /// {
        ///    tssStatus.Text = "开始任务...";
        /// }));
        /// CrossThreadCalls.SafeInvoke(this.rtxtChat, new CrossThreadCalls.TaskDelegate(delegate()
        /// {
        ///     rtxtChat.AppendText("测试中");
        /// }));
        /// 参考：http://wenku.baidu.com/view/f0b3ac4733687e21af45a9f9.html
        /// <summary>
        public static void SafeInvoke(Control control, TaskDelegate handler)
        {
            if (control.InvokeRequired)
            {
                while (!control.IsHandleCreated)
                {
                    if (control.Disposing || control.IsDisposed)
                        return;
                }
                IAsyncResult result = control.BeginInvoke(new InvokeMethodDelegate(SafeInvoke), new object[] { control, handler });
                control.EndInvoke(result);//获取委托执行结果的返回值
                return;
            }
            IAsyncResult result2 = control.BeginInvoke(handler);
            control.EndInvoke(result2);
        }

        /// <summary>
        /// 线程安全访问控件,扩展方法.net3.5用Lambda简化跨线程访问窗体控件,避免重复的delegate,Invoke
        /// this.statusStrip1.SafeInvoke(() =>
        /// {
        ///     tsStatus.Text = "开始任务....";
        /// });
        /// this.rtxtChat.SafeInvoke(() =>
        /// {
        ///     rtxtChat.AppendText("测试中");
        /// });
        /// </summary>
        //public static void SafeInvoke(this Control control, TaskDelegate handler)
        //{
        //    if (control.InvokeRequired)
        //    {
        //        while (!control.IsHandleCreated)
        //        {
        //            if (control.Disposing || control.IsDisposed)
        //                return;
        //        }
        //        IAsyncResult result = control.BeginInvoke(new InvokeMethodDelegate(SafeInvoke), new object[] { control, handler });
        //        control.EndInvoke(result);//获取委托执行结果的返回值
        //        return;
        //    }
        //    IAsyncResult result2 = control.BeginInvoke(handler);
        //    control.EndInvoke(result2);
        //}
    }
}
/*******更正一个我发现的C#多线程安全访问控件普遍存在的问题，仅供参考，在网上搜索多线程访问控件，发现很多都是这种类似的写法

http://msdn.microsoft.com/zh-cn/library/ms171728.aspx

复制代码
    private void SetText(string text)
    {
        // InvokeRequired required compares the thread ID of the
        // calling thread to the thread ID of the creating thread.
        // If these threads are different, it returns true.
        if (this.textBox1.InvokeRequired)
        {
            SetTextCallback d = new SetTextCallback(SetText);
            this.Invoke(d, new object[] { text });
        }
        else
        {
            this.textBox1.Text = text;
        }
    }
复制代码
注意红色部分，这样写几个线程同时操作时问题不是很大，但是当我几10个几100个线程频繁操作时，就出现了System.OutOfMemoryException这个异常，猜测可能是线程堵塞，同时造成cpu很高，内存成倍增长*****/